extends CanvasLayer

var selected = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	enter_selection(selected)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	selection()
	color_swaping()

func color_swaping():
	pass
	
func get_letters():
	pass
	

func selection():
	if Input.is_action_just_pressed("ui_down") or Input.is_action_just_pressed("ui_right"):
		swap_selection(selected, (selected+1)%4)
	if Input.is_action_just_pressed("ui_up") or Input.is_action_just_pressed("ui_left"):
		swap_selection(selected, (selected+3)%4)
	if Input.is_action_just_pressed("ui_accept"):
		pressed_selection(selected)
		

func swap_selection(old_selec, new_selec):
	exit_selection(old_selec)
	enter_selection(new_selec)
	selected = new_selec
	
func pressed_selection(selec):
	if selec == 0:
		_on_Campagne_pressed()
	if selec == 1:
		_on_VS_pressed()
	if selec == 2:
		_on_Options_pressed()
	if selec == 3:
		_on_Quitter_pressed()
	

func enter_selection(selec):
	if selec == 0:
		_on_Campagne_mouse_entered()
	if selec == 1:
		_on_VS_mouse_entered()
	if selec == 2:
		_on_Options_mouse_entered()
	if selec == 3:
		_on_Quitter_mouse_entered()

func exit_selection(selec):
	if selec == 0:
		_on_Campagne_mouse_exited()
	if selec == 1:
		_on_VS_mouse_exited()
	if selec == 2:
		_on_Options_mouse_exited()
	if selec == 3:
		_on_Quitter_mouse_exited()

func swap_texture(button):
	if button.text.find('['):
		button.text.erase(0,'[')
		button.text.erase(0, ']')
	else:
		button.text = '[' + button.text + ']'
	#var _swap = button.icon.get_frame_texture(0)
	#button.icon.set_frame_texture(0,button.icon.get_frame_texture(1))
	#button.icon.set_frame_texture(1,_swap)

func _on_Campagne_mouse_entered():
	swap_texture($Campagne)
func _on_Campagne_mouse_exited():
	swap_texture($Campagne)
func _on_Campagne_pressed():
	pass

func _on_VS_mouse_entered():
	swap_texture($VS)
func _on_VS_mouse_exited():
	swap_texture($VS)
func _on_VS_pressed():
	get_tree().change_scene("res://scenes/ChoixIA.tscn")

func _on_Options_mouse_entered():
	swap_texture($Options)
func _on_Options_mouse_exited():
	swap_texture($Options)
func _on_Options_pressed():
	pass

func _on_Quitter_mouse_entered():
	swap_texture($Quitter)
func _on_Quitter_mouse_exited():
	swap_texture($Quitter)
func _on_Quitter_pressed():
	get_tree().quit()




