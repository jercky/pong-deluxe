extends Control

var checked = 0

func _on_check(node_id):
	var children = get_children()
	var it = 0
	for c in children:
		if it != node_id:
			c.pressed = false
		it += 1
	checked = node_id
