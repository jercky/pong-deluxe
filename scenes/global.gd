extends Node

var tex1
var tex2
var texb 
var matbri1
var matbri2

var niv_ia

func loadField(_tex1, _tex2, _texb, _matbri1, _matbri2, _niv_ia):
	tex1 = _tex1
	tex2 = _tex2
	texb = _texb
	matbri1 = _matbri1
	matbri2 = _matbri2
	niv_ia = _niv_ia
