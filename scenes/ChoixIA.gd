extends Control

const BResult = preload("res://scenes/BResultats.gd")

func _ready():
	$Barres/j1/b1.pressed = true
	$Barres/j2/b1.pressed = true
	$Balles/b/b1.pressed  = true

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		loadField()

func _on_Button_pressed():
	loadField()

func loadField():
	var b1 = get_node("Barres/j1")
	var b2 = get_node("Barres/j2")
	var b0 = get_node("Balles/b")
	var tex1 = $Barres.get_children()[b1.checked].texture
	var tex2 = $Barres.get_children()[b2.checked].texture
	var texb = $Balles.get_children()[b0.checked].texture
	var matbri1 = [[0,0,0,0],[0,0,0,0],[1,1,1,1]]
	var matbri2 = [[0,0,0,0],[0,0,0,0],[1,1,1,1]]
	
	global.loadField(tex1,tex2,texb,matbri1,matbri2, $label_ia/niveau_ia.value)
	get_tree().change_scene("res://scenes/Field.tscn")
	


