extends Control
export (PackedScene) var Balle
export (PackedScene) var Barre

var BarrePlayer = load("res://scenes/BarrePlayer.gd")
var BarreIA = load("res://scenes/BarreIA.gd")

signal goal_p1
signal goal_p2

var ball = null
var p1 = null
var p2 = null

var texb
var rotspd = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	loadVS(global.tex1,global.tex2, global.texb, global.matbri1, global.matbri2)
	pass

func loadVS(_tex1, _tex2, _texb, _matbri1, _matbri2):
	$DebutTimer.start()
	$HUD/Message.text = "Prêt !"
	$HUD/Message.visible = true
	var screen_size = get_viewport_rect().size
	texb = _texb
	
	p1 = Barre.instance()
	p1.set_script(BarrePlayer)
	p1.initBarre("ui_up", "ui_down", 50, screen_size.y/2, _tex1)
	add_child(p1)
	
	p2 = Barre.instance()
	p2.set_script(BarreIA)
	p2.initBarre("ui_up", "ui_down", screen_size.x-50, screen_size.y/2, _tex2)
	add_child(p2)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var screen_size = get_viewport_rect().size
	# React to a goaled ball
	if (ball != null):
		if (ball.position.x < 20 || ball.position.x > screen_size.x-20):
			if (ball.position.x < 20):
				emit_signal("goal_p1")
			if (ball.position.x > screen_size.x-20):
				emit_signal("goal_p2")
			ball.goal()
			remove_child(ball)
			p2.stop()
			ball = null
			$DebutTimer.start()

# Load ball
func _on_DebutTimer_timeout():
	$HUD/Message.visible = false
	ball = Balle.instance()
	p2.run()
	p2.setBalle(ball)
	# Load texture and rotation speed
	ball.set_texture(texb)
	ball.set_rotation_speed(rotspd)
	
	add_child(ball)
	$DebutTimer.stop()

func _on_game_over():
	var t = Timer.new()
	t.set_wait_time(2)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t,"timeout")
	get_tree().change_scene("res://scenes/TitleScreen.tscn")
	

func _on_Field_goal_p1():
	$HUD/Message.visible = true
	$HUD/ScoreP2.set_text(str(int($HUD/ScoreP2.get_text())+ 1))
	if int($HUD/ScoreP2.get_text()) == 5:
		$HUD/Message.text = "Perdu !"
		_on_game_over()

func _on_Field_goal_p2():
	$HUD/Message.visible = true
	$HUD/ScoreP1.set_text(str(int($HUD/ScoreP1.get_text())+ 1))
	if int($HUD/ScoreP1.get_text()) == 5:
		$HUD/Message.text = "Gagné !"
		_on_game_over()
