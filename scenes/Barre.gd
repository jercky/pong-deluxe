extends RigidBody2D

var speed = 500
var velo_sign = 0
var prev_velo = 0
var up_key = ""
var dwn_key = ""

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
func init(_up, _dwn, _pos_x, _pos_y, _texture):
	up_key = _up
	dwn_key = _dwn
	position.x = _pos_x
	position.y = _pos_y
	$SpriteBarre.set_texture(_texture)

func initBarre(_up, _dwn, _pos_x, _pos_y, _texture):
	pass
	
func _process(delta):
	move()

func move():
	set_linear_velocity(Vector2(0,0))
	var velocity = 0
	$SpriteBarre/Animation.play("idle")
	if Input.is_action_pressed(up_key):
		velocity -= speed
	if Input.is_action_pressed(dwn_key):
		velocity += speed

	move_default(velocity)

	
func move_default(velocity):
	if velocity != 0:
		var screen_size = get_viewport_rect().size
		if ((position.y < 100 && velocity < 0)
			|| (position.y > screen_size.y-100 && velocity > 0)):
			set_linear_velocity(Vector2(0,0))
		else:
			set_linear_velocity(Vector2(0,velocity))
		$SpriteBarre.set_frame(0)
		$SpriteBarre/Animation.stop()
	return velocity

func move_icy(velocity):
	# Utilise prev_velo pour créer
	# une vitesse de déplacement
	# constante mais dégressive
	if (abs(prev_velo) > 0.005):
		prev_velo *= 0.995
		if (velocity != 0):
			prev_velo *= -1
			velo_sign = sign(velocity)
		else:
			prev_velo = abs(prev_velo)*velo_sign
	else:
		prev_velo = velocity

	velocity += prev_velo
	velocity = move_default(velocity)
	return velocity
