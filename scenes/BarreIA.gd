extends "res://scenes/Barre.gd"

var balle
var active = false
var velocity = 250

func _ready():
	pass
	
func setBalle(_balle):
	balle = _balle
	
func run():
	active = true
func stop():
	active = false

func initBarre(_up, _dwn, _pos_x, _pos_y, _texture):
	init("","",_pos_x,_pos_y,_texture)

func predict():
	var pos_balle = balle.position
	velocity = balle.position.y - position.y
	velocity = velocity *global.niv_ia/2

func move():
	if (active):
		predict()
		move_default(velocity)
	else:
		var screen_size = get_viewport_rect().size
		var velocity = 250
		if (abs(position.y-screen_size.y/2) > 50):
			var side = -int(position.y-screen_size.y/2 > 0)*2+1
			move_default(velocity*side)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
