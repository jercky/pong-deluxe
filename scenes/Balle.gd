extends Area2D

var velocity = Vector2(-0.5,0)
var m_rotation = Transform2D()
var b_rotation = 0
var rot_speed = 2

signal hit


# Called when the node enters the scene tree for the first time.
func _ready():
	var screen_size = get_viewport_rect().size
	position.x = screen_size.x/2
	position.y = screen_size.y/2

	randomize()
	b_rotation =  45+(90*(((randi()%4)*2-3)/2.0+0.5))+randi()%15
	
	m_rotation = m_rotation.rotated(deg2rad(b_rotation))
	var t_velo = Vector2()
	t_velo.x = m_rotation[0][0]*velocity.x+m_rotation[0][1]*velocity.y
	t_velo.y = m_rotation[1][0]*velocity.x+m_rotation[1][1]*velocity.y
	velocity = t_velo

	$SpeedUpTimer.start()

func set_texture(texture):
	$Sprite.set_texture(texture)
	
func set_rotation_speed(_rotspd):
	rot_speed = _rotspd

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var screen_size = get_viewport_rect().size
	if (position.y + velocity.y < 50 ||
		position.y + velocity.y > screen_size.y-50):
		velocity.y *= -1
		b_rotation = int(b_rotation+180)%360
	rotation_degrees += rot_speed *velocity.x
	position += velocity*delta*1000

func goal():
	queue_free()

func _on_Area2D_body_entered(body):
	# Quand la balle touche un obstacle horizontal, elle rebondit...
	velocity.x *= -1
	# ... et dévit aléatoirement de sa trajectoire.
	randomize()
	var fact = (randi()%100-50)/25
	if fact == 0:
		fact = 1
	velocity.y *= fact
	
	# b_rotation = int(b_rotation+90)%360


func _on_SpeedUpTimer_timeout():
	velocity*=1.5
